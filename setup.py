#!/usr/bin/python3
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import re


setup(
	name='pycontrolapi_rpyc_service',
	version=0.0,
	author='Ricardo Jorge Vieira Ribeiro',
	author_email='cajomferro@gmail.com, ricardojvr@gmail.com',
	license='Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>',
	url='https://bitbucket.org/fchampalimaud/pycontrol-api',

	include_package_data=True,
	packages=find_packages(exclude=['contrib', 'docs', 'tests', 'examples', 'deploy', 'reports']),

	# install_requires=requirements,
	entry_points={
		'console_scripts':[
			'pycontrol-rpyc-service=pycontrolapi_rpyc_service.__main__:start_service',
		]
	}
)
