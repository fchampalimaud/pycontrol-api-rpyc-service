from pysettings import conf;

# load the user settings in case the file exists
try:
	import user_settings
	conf += user_settings
except:
	pass
