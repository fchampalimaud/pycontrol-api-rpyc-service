# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
import rpyc

from pycontrolapi_rpyc_service.models.base_service import BaseService
from pycontrolapi.models.task import Task

class TaskService(BaseService, Task):
	
	def _rpyc_getattr(self, name):
		print('get:TaskService', self, name)
		return getattr(self, name)

	def _rpyc_setattr(self, name, value):
		print('set:TaskService', self, name, value)
		setattr(self, name, value)