# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
import rpyc

from pycontrolapi_rpyc_service.models.base_service import BaseService
from pycontrolapi.models.setup import Setup
from pycontrolapi_rpyc_service.models.session import Session
from pycontrolapi_rpyc_service.models.setup.board_task import BoardTask

class SetupService(BaseService, Setup):
	pass


	def create_session(self): return Session(self)

	def create_board_task(self): return BoardTask(self)