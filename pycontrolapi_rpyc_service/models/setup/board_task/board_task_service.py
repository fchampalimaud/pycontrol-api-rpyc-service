# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
import rpyc

from pycontrolapi_rpyc_service.models.base_service import BaseService
from pycontrolapi.models.setup.board_task import BoardTask

class BoardTaskService(BaseService, BoardTask):
	
	pass