# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
import rpyc

from pycontrolapi.models.board.board_operations import BoardOperations
from pycontrolapi_rpyc_service.models.base_service import BaseService
from pycontrolapi.models.board import Board

class BoardService(BaseService, Board):



	def install_framework(self, framework_path, close=True):
		self.install_framework_activated = True	

		self.install_queue = []
		Board.install_framework(self, framework_path, close)

	def install_framework_handler_evt(self, e, result):		
		try:
			Board.install_framework_handler_evt(self, e, result)
		except Exception as ex: 
			result = ex
		self.install_queue.append( (e, result) )

		if e.extra_args[0] == BoardOperations.INSTALLFRAMEWORK_IMPORT_PYCONTROL:			
			del self.install_framework_activated


	def install_task(self, board_task, close=True):pass
	def install_task_handler_evt(self, e, result):pass

	def sync_variables(self, board_task, close=True, func_group_id=None):pass
	def sync_variables_handler_evt(self, e, result):pass
	
	def run_task(self, session, board_task, close=True):pass
	def run_task_handler_evt(self, e, result):pass