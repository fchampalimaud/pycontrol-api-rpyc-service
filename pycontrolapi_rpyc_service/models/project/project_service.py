# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
import rpyc

from pycontrolapi_rpyc_service.models.base_service import BaseService
from pycontrolapi.models.project import Project

from pycontrolapi_rpyc_service.models.experiment import Experiment
from pycontrolapi_rpyc_service.models.board import Board
from pycontrolapi_rpyc_service.models.task import Task

class ProjectService(BaseService, Project):
	

	
	def create_experiment(self): return Experiment(self )

	def create_board(self): return Board(self )

	def create_task(self): return Task(self )
