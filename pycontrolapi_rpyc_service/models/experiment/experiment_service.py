# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pysettings import conf
import rpyc

from pycontrolapi_rpyc_service.models.base_service import BaseService
from pycontrolapi.models.experiment import Experiment
from pycontrolapi_rpyc_service.models.setup import Setup

class ExperimentService(BaseService, Experiment):
	
	"""
	def __init__(self, project):
		Experiment.__init__(self,project)
		BaseService.__init__(self)
	"""

	def create_setup(self): return Setup(self)