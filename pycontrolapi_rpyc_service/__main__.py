import rpyc, sys
rpyc.core.protocol.DEFAULT_CONFIG['allow_pickle'] = True
from rpyc.utils.server import ThreadedServer, OneShotServer, ForkingServer

from pycontrolapi_rpyc_service.models.project import Project

project = Project()
project.load(sys.argv[1] if len(sys.argv)>1 else '/home/ricardo/Downloads/teste/Proj1')


class ProjectService(rpyc.SlaveService):

	
	def on_connect(self):
		print("connected")

	def on_disconnect(self):
		print("disconnected")

	@property
	def exposed_project(self):		
		global project
		return project
	@exposed_project.setter
	def exposed_project(self, value): 
		global project
		project = value
		

def start_service():
	t = ThreadedServer(ProjectService, port=18861, protocol_config = {"allow_all_attrs": True, 'allow_pickle':True})
	t.start()

if __name__ == "__main__": start_service()